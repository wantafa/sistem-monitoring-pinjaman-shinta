@extends('backend.index')
@section('title', 'Laporan')
@section('isi-konten')

<div class="container-xxl flex-grow-1 container-p-y">

    <h4 class="fw-bold py-3 mb-4">
        <span class="text-muted fw-light">Dashboard /</span> Laporan
    </h4>

    <button class="btn btn-primary add-btn mb-3" data-toggle="modal" data-target="#modal_peminjam"> <i class="bx bx-plus bx-xs"></i> Tambah</button>

    <div class="row">
        {{-- <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4>Laporan Pinjaman</h4>
                </div>
                <div class="card-body">

                    <form action="{{ route('laporan') }}" method="GET" class="form-group" id="formFilter">
                        @csrf
                        <label for="peminjam_id" class="form-label">Nama Peminjam</label>

                        <select style="cursor:pointer;" class="form-control" id="peminjam_id" name="peminjam_id">
                            <option value="">- Pilih -</option>
                            @foreach ($get_peminjam as $item)
                            <option value="{{ $item->id }}">{{ $item->nama_peminjam }}</option>
                            @endforeach
                        </select>

                        <label for="pinjaman" class="form-label">Status Pinjaman</label>
                        <select style="cursor:pointer;" class="form-control col-6" id="pinjaman" name="pinjaman">
                            <option value="">- Pilih -</option>
                            <option value="verif">Verification</option>
                            <option value="pinjaman">Pinjaman</option>
                            <option value="pending">Pending</option>
                            <option value="paid">Paid</option>
                        </select>

                        <label for="tgl_jatuh_tempo" class="form-label">Tanggal Jatuh Tempo</label>
                        <input type="date" id="tgl_jatuh_tempo" name="tgl_jatuh_tempo" class="form-control">

                        <label for="tgl_pembayaran" class="form-label">Tanggal Pembayaran</label>
                        <input type="date" id="tgl_pembayaran" name="tgl_pembayaran" class="form-control mb-2">

                    </form>
                    <button class="btn btn-info btn-block" type="submit" form="formFilter" value="Submit">Cari
                        Data</button>
                </div>
            </div>
        </div> --}}
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4>Laporan Cheque</h4>
                </div>
                <div class="card-body">

                    <form action="{{ route('laporan') }}" method="GET" class="form-group" id="cheque">
                        @csrf
                        <input type="hidden" value="cheque" name="pinjaman">
                        <div class="row">
                        <div class="col-md-3">
                            <label for="peminjam_id" class="form-label">Nama Peminjam</label>
                            <select style="cursor:pointer;" class="form-control col-6" id="peminjam_id" name="peminjam_id">
                                <option value="">- Pilih -</option>
                                @foreach ($get_peminjam as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_peminjam }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="tgl_terima_cheque" class="form-label">Tanggal Penerimaan</label>
                            <input type="date" id="tgl_terima_cheque" name="tgl_terima_cheque" class="form-control">
                        </div>
                        <div class="col-md-3">
                            <label for="nominal_cheque" class="form-label">Nominal Cheque</label>
                            <input type="number" min="0" id="nominal_cheque" name="nominal_cheque" class="form-control" placeholder="Masukkan Nominal Cheque">
                        </div>
                        <div class="col-md-3">
                            <label for="status" class="form-label">Status Cheque</label>
                            <select name="status" id="status" class="form-control mb-2">
                                <option value="">- Pilih -</option>
                                <option value="CO VT">CO VT</option>
                                <option value="Finance">Finance</option>
                            </select>
                        </div>
                    </div>
                    </form>
                    <button class="btn btn-info btn-block" type="submit" form="cheque" value="Submit"><i class="bx bx-search"></i> Cari Data</button>

                        <div class="col-md-12 text-end mb-3">
                            <a href="{{ route('cetak.laporan.cheque') }}" target="_blank" class="btn btn-primary"><i class="bx bx-export mb-1"></i> Cetak</a>
                        </div>
                        <div class="table-responsive text-nowrap">
                            <table class="table table-hover table-striped" id="cheque-datatable">
                                <thead>
                                    <tr class="table-secondary">
                                        <th>No</th>
                                        <th>Tipe</th>
                                        <th>Bank</th>
                                        <th>Nomor Rekening</th>
                                        <th>Tempat Pembayaran</th>
                                        <th>Rekening Tujuan</th>
                                        <th>Atas Nama</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cheque as $item)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $item->tipe_cheque}}</td>
                                        <td>{{ $item->bank ?? null}}</td>
                                        <td>{{ $item->no_rek ?? null}}</td>
                                        <td>{{ $item->tempat_pembayaran ?? null}}</td>
                                        <td>{{ $item->rek_tujuan ?? null}}</td>
                                        <td>{{ $item->atas_nama ?? null}}</td>
                                        <td><a href="{{ route('cetak.cheque', $item->id) }}" target="_blank"><i class="btn btn-lg bx bx-export mb-1 text-dark"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mt-5">
        {{-- <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4>Data Laporan Pinjaman</h4>
                </div>
                <div class="card-body">
                    <div class="col-md-12 text-end mb-3">
                        <a href="" target="_blank" class="btn btn-primary">Cetak <i class="fa fa-print"></i></a>
                    </div>
                    <div class="table-responsive text-nowrap">
                        <table class="table table-hover table-striped" id="my-datatable">
                            <thead>
                                <tr class="table-secondary">
                                    <th>No</th>
                                    <th>Nama Peminjam</th>
                                    <th>Nominal Pinjaman</th>
                                    <th>Status Pembayaran</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pinjaman as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->nama_peminjam}}</td>
                                    <td>{{ $item->nominal_pinjaman ?? null}}</td>
                                    <td>{{ $item->status_pembayaran ?? null}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> --}}
        {{-- <div class="col-6">
            <div class="card">
                <div class="card-header">
                    <h4>Data Laporan Cheque</h4>
                </div>
                <div class="card-body">
                    <div class="col-md-12 text-end mb-3">
                        <a href="" target="_blank" class="btn btn-primary">Cetak <i class="fa fa-print"></i></a>
                    </div>
                    <div class="table-responsive text-nowrap">
                        <table class="table table-hover table-striped" id="cheque-datatable">
                            <thead>
                                <tr class="table-secondary">
                                    <th>No</th>
                                    <th>Tipe</th>
                                    <th>Bank</th>
                                    <th>Nomor Rekening</th>
                                    <th>Tempat Pembayaran</th>
                                    <th>Rekening Tujuan</th>
                                    <th>Atas Nama</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($cheque as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->tipe_cheque}}</td>
                                    <td>{{ $item->bank ?? null}}</td>
                                    <td>{{ $item->no_rek ?? null}}</td>
                                    <td>{{ $item->tempat_pembayaran ?? null}}</td>
                                    <td>{{ $item->rek_tujuan ?? null}}</td>
                                    <td>{{ $item->atas_nama ?? null}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>

</div>

@endsection
@push ('page-scripts')
<script>
    $(document).ready(function() {
        var table = $("#my-datatable").DataTable();
        var table = $("#cheque-datatable").DataTable();
    });
</script>
@endpush