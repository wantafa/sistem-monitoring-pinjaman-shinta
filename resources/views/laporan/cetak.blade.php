<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PAID INVOICE</title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 10px;
        margin-right: auto;
    }

    th {
        border: 1px solid;
    }

</style>
<body>
    <img src="{{ public_path('assets/img/logo.jpeg') }}" height="10%">
    <div style="text-align: center;font-size:18px;margin-top:10px;margin-bottom:20px;">
        <span><strong> <u>PAID INVOICE</u></strong></span><br>
        <span class="center" >2023</span>
    </div>


    <p style="font-weigth: bold;"><b>PINJAMAN</b></p>

    <table class="tbl-center">
        <tr>
            <td>Nomor Pinjaman</td>
            <td> :</td>
            <td>{{ $cetak->no_pinjaman }}</td>
        </tr>
        <tr>
            <td>Nama Perusahaan</td>
            <td> :</td>
            <td>{{ $cetak->nama_peminjam }}</td>
        </tr>        
        <tr>
            <td>Tipe Pinjaman</td>
            <td> :</td>
            <td>{{ $cetak->nama }}</td>
        </tr>        
        <tr>
            <td>Tanggal Pencairan</td>
            <td> :</td>
            <td>{{ \Carbon\Carbon::parse($cetak->tgl_pencairan)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>Tanggal Jatuh Tempo</td>
            <td> :</td>
            <td>{{ \Carbon\Carbon::parse($cetak->tgl_jatuh_tempo)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>Nominal Pinjaman</td>
            <td> :</td>
            <td>@duit($cetak->nominal_pinjaman)</td>
        </tr>        
        <tr>
            <td>Tanggal Validasi</td>
            <td> :</td>
            <td>{{ $cetak->tgl_validasi }}</td>
        </tr>        

    </table>

    <p style="text-align: justify;"><b>INVOICE</b></p>

    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0; width: 100%}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
          overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
          font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0lax{text-align:center;vertical-align:top}
        .tg .tg-isi{text-align:left;vertical-align:top}
        </style>
        <table class="tg">
        <thead>
          <tr>
            <th class="tg-0lax">Nomor Invoice</th>
            <th class="tg-0lax">Tanggal Pembayaran</th>
            <th class="tg-0lax">Nama Payor</th>
            <th class="tg-0lax">Nominal Invoice</th>
            <th class="tg-0lax">Note</th>
            <th class="tg-0lax">Status</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($invoice as $item)
                <tr>
                    <td class="tg-isi">{{ $item->no_invoice }}</td>
                    <td class="tg-isi">{{ \Carbon\Carbon::parse($item->tgl_pembayaran)->translatedFormat('d F Y') }}</td>
                    <td class="tg-isi">{{ $cetak->nama_payor }}</td>
                    <td class="tg-isi">@duit($item->nominal_invoice)</td>
                    <td class="tg-isi">{{ $item->note }}</td>
                    <td class="tg-isi">{{ $item->status }}</td>
                </tr>
            @endforeach
        </tbody>
        </table>
</body>
</html>