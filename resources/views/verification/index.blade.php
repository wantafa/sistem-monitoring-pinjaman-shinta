@extends('backend.index')
@section('title', 'Data Pinjaman')
@section('isi-konten')
<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">

    <h4 class="fw-bold py-3 mb-4">
        <span class="text-muted fw-light">Dashboard /</span> Invoice Pending Verification
    </h4>

    {{-- <button class="btn btn-primary add-btn mb-3" data-toggle="modal" data-target="#modal_pinjaman"> <i class="bx bx-plus bx-xs"></i> Tambah</button> --}}

    <!-- Bordered Table -->
    <div class="card">
        <h5 class="card-header">Data Pinjaman</h5>
        <div class="card-body">
            <div class="table-responsive text-nowrap">
                <table class="table table-bordered" id="my-datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tipe Pinjaman</th>
                            <th>Nomor Pinjaman</th>
                            <th>Tanggal Pencairan</th>
                            <th>Tanggal Jatuh Tempo</th>
                            <th>Nama Payor</th>
                            <th>Status Pembayaran</th>
                            @if (Auth::user()->role == 'vt')
                            <th width="80">Actions</th>
                            @endif
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!--/ Bordered Table -->
</div>
<!-- / Content -->

{{-- Modal start --}}

{{-- <div id="modal_pinjaman" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Tambah</span> Risk Register</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                <form method="POST" action="">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label class="col-form-label">Nama<span class="text-danger">*</span></label>
                        <input class="form-control" id="nama" name="nama" type="text" required>
                    </div>
                    <div class="form-group mb-3">
                        <label class="col-form-label">Kategori<span class="text-danger">*</span></label>
                        <input class="form-control" id="kategori" name="kategori" type="text" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="modal_pinjaman" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalCenterTitle"><span>Edit</span> Pinjaman</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{ route('update.verif') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="id" name="id">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <label for="tgl_validasi" class="form-label">Tanggal Validasi <span class="text-danger">*</span></label>
                        <input type="date" id="tgl_validasi" name="tgl_validasi" class="form-control" required>
                    </div>
                    <div class="col-md-12 mb-2">
                        <label for="note" class="form-label">Note</label>
                        <input type="text" id="note" name="note" class="form-control">
                    </div>
                    <div class="col-md-12 mb-2">
                        <label for="status_pembayaran" class="form-label">Status Pembayaran</label>
                        <select name="status_pembayaran" id="status_pembayaran" class="form-control">
                            <option value="">- Pilih -</option>
                            <option value="Paid">Paid</option>
                        </select>
                    </div>
                </div>
            </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button> --}}
                    <button class="btn btn-primary">Update</button>
                </div>
            </form>
    </div>
  </div>
</div>

{{-- Modal end --}}

{{-- Modal Detail start --}}

<div class="modal fade" id="modal_pinjaman_detail" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                    <h5 class="modal-title" id="modalCenterTitle">Detail Pinjaman</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
                <div class="modal-body">
                        <table class="table table-border">
                                <tr>
                                    <td>Nomor Pinjaman</td>
                                    <td>:</td>
                                    <td class="text-end" id="no_pinjaman"></td>
                                </tr>
                                <tr>
                                    <td>Nama Peminjam</td>
                                    <td>:</td>
                                    <td class="text-end" id="nama_peminjam"></td>
                                </tr>
                                <tr>
                                    <td>Tipe Pinjaman</td>
                                    <td>:</td>
                                    <td class="text-end" id="nama"></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Pencairan</td>
                                    <td>:</td>
                                    <td class="text-end" id="tgl_pencairan"></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Jatuh Tempo</td>
                                    <td>:</td>
                                    <td class="text-end" id="tgl_jatuh_tempo"></td>
                                </tr>
                                <tr>
                                    <td>Nominal Pinjaman</td>
                                    <td>:</td>
                                    <td class="text-end" id="nominal_pinjaman"></td>
                                </tr>
                                <tr>
                                    <td>Nama Payor</td>
                                    <td>:</td>
                                    <td class="text-end" id="nama_payor"></td>
                                </tr>
                                <tr>
                                    <td>Nomor Invoice</td>
                                    <td>:</td>
                                    <td class="text-end" id="no_invoice"></td>
                                </tr>
                                <tr>
                                    <td>Dokumen</td>
                                    <td>:</td>
                                    <td class="text-end" id="dokumen"></td>
                                </tr>
                                <tr>
                                    <td>Tanggal Validasi</td>
                                    <td>:</td>
                                    <td class="text-end" id="tgl_validasi"></td>
                                </tr>
                        </table>
            </div>
        </div>
    </div>
</div>

{{-- modal end --}}

@endsection
@push ('page-scripts')
{{-- <script>
        var table = $("#my-datatable").DataTable();

</script> --}}

<script>
    $(document).ready(function() {
        const id_peminjam = $(this).data("id");

        var table = $("#my-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('verif.json', ['id'=>$peminjam->id]) }}",
            columns: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                },
                {
                    data: "nama",
                    name: "nama",
                    // render: function(data, type, row) {
                    //     var url = "{{ url('pkpt/risk-register') }}/"+row.id;
                    //     return `<a href="${url}">${data}</a>`;
                    // }
                },
                {
                    data: "no_pinjaman",
                    name: "no_pinjaman"
                },
                {
                    data: "tgl_pencairan",
                    name: "tgl_pencairan"
                },
                {
                    data: "tgl_jatuh_tempo",
                    name: "tgl_jatuh_tempo"
                },
                {
                    data: "nama_payor",
                    name: "nama_payor"
                },
                {
                    data: "status_pembayaran",
                    name: "status_pembayaran"
                },
                @if (Auth::user()->role == 'vt')
                {
                    data: "action",
                    name: "action",
                    className: 'text-center'
                },
                @endif
            ],
            order: [[ 0, "desc" ]]
        });

        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // add pinjaman
        $('.add-btn').on('click', function(){            
            $('#modal_pinjaman .modal-title span').html('Tambah');
            $('#modal_pinjaman #id').val('');
            $('#modal_pinjaman form').trigger('reset');


            var myModal = new bootstrap.Modal(document.getElementById('modal_pinjaman'))
            myModal.show()
        });

        // edit pinjaman
        $("#my-datatable").on("click", ".btn-edit", function(e) {
            e.preventDefault();
            data = $(this).data();

            $.each(data, function(index, value){

                $('#modal_pinjaman #'+index).val(value);
            })


            $('#modal_pinjaman'+' .modal-title span').html('Edit');

            var myModal = new bootstrap.Modal(document.getElementById('modal_pinjaman'))
            myModal.show()
        });

        // detail pinjaman
        $("#my-datatable").on("click", ".btn-detail", function(e) {
            e.preventDefault();
            data = $(this).data();
    
            $.each(data, function(index, value){
            if (index === 'dokumen') {
            
                var url = window.location.origin + '/' + value;
                var link = '<a href="' + url + '" target="_blank" class="btn btn-success btn-sm">Lihat</a>';

                $('#modal_pinjaman_detail #'+index).html(link);

            } else if (index === 'nominal_pinjaman') { 

                var rupiah = formatRupiah(value);
                $('#modal_pinjaman_detail #'+index).html(rupiah);

            } else if (index === 'no_invoice') {
                
                var no_invoice = value.split(",");
                var enter_kebawah = no_invoice.join("<br>");

                $('#modal_pinjaman_detail #'+index).html(enter_kebawah);

            } else  {

                $('#modal_pinjaman_detail #'+index).html(value);
            }
            })
            
            // Fungsi untuk mengubah nilai menjadi format rupiah
            function formatRupiah(value) {
                var formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 0 
                });
                return formatter.format(value);
            }            
            
                var myModal = new bootstrap.Modal(document.getElementById('modal_pinjaman_detail'))
            myModal.show()
            
        });

    });

</script>
@endpush
