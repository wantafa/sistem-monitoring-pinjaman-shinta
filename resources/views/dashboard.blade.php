@extends('backend.index')
@section('title', 'Dashboard')
@section('isi-konten')
    <!-- Content -->
    <div class="container-xxl flex-grow-1 container-p-y">
        <div id="MyClockDisplay" class="clock" onload="showTime()" style="float: right"></div>
        <div class="alert alert-info" role="alert">
            <i class="bx bx-bell"></i>
            Hallo, {{ Auth::user()->name }} <br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Selamat Datang Di SIstem Monitoring Dokumen Jaminan</b>
          </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 order-1">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <li class="d-flex pb-1">
                                    <div class="avatar flex-shrink-0 me-3 mt-n1">
                                        {{-- <img src="../assets/img/icons/unicons/paypal.png" alt="User" class="rounded" /> --}}
                                        <i class="bx bxs-user-circle text-info h1"></i>
                                    </div>
                                    <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                        <div class="me-2">
                                            <small class="d-block mb-1">Peminjam</small>
                                            <h6 class="mb-0">{{ $peminjam }}</h6>
                                        </div>
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <li class="d-flex pb-1">
                                    <div class="avatar flex-shrink-0 me-3 mt-n1">
                                        {{-- <img src="../assets/img/icons/unicons/paypal.png" alt="User" class="rounded" /> --}}
                                        <i class="bx bxs-folder-open text-primary h1"></i>
                                    </div>
                                    <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                        <div class="me-2">
                                            <small class="d-block mb-1">Pinjaman</small>
                                            <h6 class="mb-0">{{ $pinjaman }}</h6>
                                        </div>
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <li class="d-flex pb-1">
                                    <div class="avatar flex-shrink-0 me-3 mt-n1">
                                        {{-- <img src="../assets/img/icons/unicons/paypal.png" alt="User" class="rounded" /> --}}
                                        <i class="bx bxs-right-top-arrow-circle text-info h1"></i>
                                    </div>
                                    <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                        <div class="me-2">
                                            <small class="d-block mb-1">Pending Verification</small>
                                            <h6 class="mb-0">{{ $verif }}</h6>
                                        </div>
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <li class="d-flex pb-1">
                                    <div class="avatar flex-shrink-0 me-3 mt-n1">
                                        {{-- <img src="../assets/img/icons/unicons/paypal.png" alt="User" class="rounded" /> --}}
                                        <i class="bx bx-loader-circle text-secondary h1"></i>
                                    </div>
                                    <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                        <div class="me-2">
                                            <small class="d-block mb-1">Pending Payment</small>
                                            <h6 class="mb-0">{{ $pending }}</h6>
                                        </div>
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6 mb-4">
                        <div class="card">
                            <div class="card-body">
                                <li class="d-flex pb-1">
                                    <div class="avatar flex-shrink-0 me-3 mt-n1">
                                        {{-- <img src="../assets/img/icons/unicons/paypal.png" alt="User" class="rounded" /> --}}
                                        <i class="bx bxs-check-circle text-success h1"></i>
                                    </div>
                                    <div class="d-flex w-100 flex-wrap align-items-center justify-content-between gap-2">
                                        <div class="me-2">
                                            <small class="d-block mb-1">Paid Invoice</small>
                                            <h6 class="mb-0">{{ $paid }}</h6>
                                        </div>
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Order Statistics -->
            <div class="col-md-6 col-lg-12 col-xl-12 order-0 mb-4">
                <div class="card h-100">
                    <div class="card-header d-flex align-items-center justify-content-between pb-0">
                        <div class="card-title mb-0">
                            <h5 class="m-0 me-2">Grafik Invoice</h5>
                            {{-- <small class="text-muted">{{ $chart_pending + $chart_paid }} Total Invoice</small> --}}
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="chart"></div>
                    </div>
                </div>
            </div>
            <!--/ Order Statistics -->

        </div>
    </div>
    <!-- / Content -->
@endsection

@push ('page-scripts')
<script>
     var options = {
          series: [{
          name: 'Pending Invoice',
          data: [ '{{ $chart_pending }}']
        }, {
          name: 'Paid Invoice',
          data: ['{{ $chart_paid }}']
        }],
          chart: {
          type: 'bar',
          height: 350
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded'
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent']
        },
        xaxis: {
          categories: ['Pending Invoice', 'Paid Invoice'],
        },
        yaxis: {
          title: {
            text: 'Invoice'
          }
        },
        fill: {
          opacity: 1
        },
        tooltip: {
          y: {
            formatter: function (val) {
              return val + " Invoice"
            }
          }
        }
        };

        var chart = new ApexCharts(document.querySelector("#chart"), options);
        chart.render();
  </script>
@endpush
