@extends('backend.index')
@section('title', 'Data Pinjaman')
@section('isi-konten')
<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">

    <h4 class="fw-bold py-3 mb-4">
        <span class="text-muted fw-light">Dashboard /</span> Pinjaman
    </h4>

    @if (Auth::user()->role == 'disbur')
    <button class="btn btn-primary add-btn mb-3" data-toggle="modal" data-target="#modal_pinjaman"> <i class="bx bx-plus bx-xs"></i> Tambah</button>
    @endif

    <!-- Bordered Table -->
    <div class="card">
        <h5 class="card-header">Data Pinjaman</h5>
        <div class="card-body">
            <div class="table-responsive text-nowrap">
                <table class="table table-bordered" id="my-datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tipe Pinjaman</th>
                            <th>Nomor Pinjaman</th>
                            <th>Tanggal Pencairan</th>
                            <th>Tanggal Jatuh Tempo</th>
                            <th>Nama Payor</th>
                            <th>Status Pembayaran</th>
                            @if (Auth::user()->role == 'disbur')
                            <th width="80">Actions</th>
                            @endif
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!--/ Bordered Table -->
</div>
<!-- / Content -->

{{-- Modal start --}}

{{-- <div id="modal_pinjaman" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Tambah</span> Risk Register</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                <form method="POST" action="">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label class="col-form-label">Nama<span class="text-danger">*</span></label>
                        <input class="form-control" id="nama" name="nama" type="text" required>
                    </div>
                    <div class="form-group mb-3">
                        <label class="col-form-label">Kategori<span class="text-danger">*</span></label>
                        <input class="form-control" id="kategori" name="kategori" type="text" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="modal_pinjaman" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalCenterTitle"><span>Tambah</span> Pinjaman</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{ route('pinjaman.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="id" name="id">
                <input type="hidden" id="peminjam_id" name="peminjam_id" value="{{ request('id') }}">
                <div class="row">
                    <div class="col-md-6 mb-2">
                        <label for="tipe_pinjaman_id" class="form-label">Tipe Pinjaman <span class="text-danger">*</span></label>

                        <select name="tipe_pinjaman_id" class="form-control" id="tipe_pinjaman_id">
                                <option value="">- Pilih -</option>
                            @foreach ($tipe_pinjaman as $item)
                                <option value="{{ $item->id }}">{{ $item->nama }} - {{ $item->kode }}</option>
                            @endforeach
                        </select>
                    </div>
                    {{-- <div class="col-md-6 mb-2">
                        <label for="no_pinjaman" class="form-label">Nomor Pinjaman <span class="text-danger">*</span></label>
                        <input type="text" id="no_pinjaman" name="no_pinjaman" class="form-control" placeholder="Masukkan Nomor Pinjaman" required>
                    </div> --}}
                    <div class="col-md-6 mb-2">
                        <label for="tgl_pencairan" class="form-label">Tanggal Pencairan <span class="text-danger">*</span></label>
                        <input type="date" id="tgl_pencairan" name="tgl_pencairan" class="form-control" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="tgl_jatuh_tempo" class="form-label">Tanggal Jatuh Tempo <span class="text-danger">*</span></label>
                        <input type="date" id="tgl_jatuh_tempo" name="tgl_jatuh_tempo" class="form-control" required>
                    </div>
                    {{-- <div class="col-md-6 mb-2">
                        <label for="tgl_pembayaran" class="form-label">Tanggal Pembayaran <span class="text-danger">*</span></label>
                        <input type="date" id="tgl_pembayaran" name="tgl_pembayaran" class="form-control" required>
                    </div> --}}
                    {{-- <div class="col-md-6 mb-2">
                        <label for="nominal_pinjaman" class="form-label">Nominal Pinjaman <span class="text-danger">*</span></label>
                        <input type="number" min="0" id="nominal_pinjaman" name="nominal_pinjaman" class="form-control" placeholder="Masukkan Nominal Pinjaman" required>
                    </div> --}}
                    <div class="col-md-6 mb-2">
                        <div class="input_no_invoice">

                            <label for="no_invoice" class="form-label">Nomor Invoice <span class="text-danger">*</span></label>
                            <div class="input-field">
                                <input type="text" id="no_invoice" name="no_invoice[]" class="form-control" placeholder="Masukkan Nomor Invoice" required>
                            </div>
                        </div>

                        <button class="btn btn-secondary btn-sm mt-2" id="add_field">Tambah</button>
                    </div>

                    <div class="col-md-6 mb-2">
                        <label for="nama_payor" class="form-label">Nama Payor</label>
                        <input type="text" id="nama_payor" name="nama_payor" class="form-control" placeholder="Masukkan Nama Payor">
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="dokumen" class="form-label">Dokumen</label>
                        <input type="file" id="dokumen" name="dokumen" class="form-control">
                    </div>
                </div>
            </div>
                <div class="modal-footer">
                    {{-- <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button> --}}
                    <button class="btn btn-primary">Simpan</button>
                </div>
            </form>
    </div>
  </div>

{{-- Modal end --}}

{{-- Delete Job Modal start --}}

<div class="modal custom-modal fade" id="delete_pinjaman" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLongTitle">Hapus</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h4 class="text-center">Anda yakin ingin hapus?</h4>
                <div class="modal-btn delete-action">
                    <div class="row">
                        <div class="col-6">
                            <form method="POST" id="url-delete">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-primary continue-btn w-100" type="submit">Delete</button>
                            </form>
                        </div>
                        <div class="col-6">
                            <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Delete Job Modal end --}}

{{-- modal end --}}
@endsection
@push ('page-scripts')
{{-- <script>
        var table = $("#my-datatable").DataTable();

</script> --}}

<script>
    $(document).ready(function() {
        const id_peminjam = $(this).data("id");

        var table = $("#my-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('pinjaman.json', ['id'=>$peminjam->id]) }}",
            columns: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                },
                {
                    data: "nama",
                    name: "nama",
                    // render: function(data, type, row) {
                    //     var url = "{{ url('pkpt/risk-register') }}/"+row.id;
                    //     return `<a href="${url}">${data}</a>`;
                    // }
                },
                {
                    data: "no_pinjaman",
                    name: "no_pinjaman"
                },
                {
                    data: "tgl_pencairan",
                    name: "tgl_pencairan",
                },
                {
                    data: "tgl_jatuh_tempo",
                    name: "tgl_jatuh_tempo",
                },
                {
                    data: "nama_payor",
                    name: "nama_payor",
                },
                {
                    data: "status_pembayaran",
                    name: "status_pembayaran"
                },
                @if (Auth::user()->role == 'disbur')
                {
                    data: "action",
                    name: "action",
                    className: 'text-center'
                },
                @endif
            ],
            order: [[ 0, "desc" ]]
        });

        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // add pinjaman
        $('.add-btn').on('click', function(){            
            $('#modal_pinjaman .modal-title span').html('Tambah');
            $('#modal_pinjaman #id').val('');
            $('#modal_pinjaman form').trigger('reset');


            var myModal = new bootstrap.Modal(document.getElementById('modal_pinjaman'))
            myModal.show()
        });

        // edit pinjaman
        $("#my-datatable").on("click", ".btn-edit", function(e) {
            e.preventDefault();
            data = $(this).data();

            $.each(data, function(index, value){

                if (index === 'tipe_pinjaman_id') {
                    $('#modal_pinjaman #'+index).val(value).change();

                } else {
                        $('#modal_pinjaman #'+index).val(value);
                }
            })

            // Dapatkan nilai no_invoice dari data
            var no_invoice = $('#no_invoice').val();
            
            var no_invoice_array = no_invoice.split(","); // Pecah nilai no_invoice menjadi array

            var x = no_invoice_array.length; // Counter untuk input field, diambil dari jumlah nomor invoice yang ada

            // Tampilkan input field sejumlah nomor invoice yang ada
            for (var i = 0; i < x; i++) {
                if (i == 0) {
                    $(wrapper).find('.input-field:first input').val(no_invoice_array[i]);
                } else {
                    $(wrapper).append('<div class="input-field input-group"><input type="text" name="no_invoice[]" id="no_invoice" class="form-control" placeholder="Masukkan Nomor Invoice ' + (i+1) + '" value="' + no_invoice_array[i] + '"><button class="btn btn-outline-primary remove_field">Hapus</button></div>');
                }
            }

            $('#modal_pinjaman').on('hide.bs.modal', function() {  
                $(wrapper).children('.input-field:not(:first)').remove(); // Hapus semua elemen kecuali yang pertama
                x = 1;
            });

            $('#modal_pinjaman'+' .modal-title span').html('Edit');

            var myModal = new bootstrap.Modal(document.getElementById('modal_pinjaman'))
            myModal.show()
        });

        // delete pinjaman
        $("#my-datatable").on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const id = $(this).data("id");
            $('#delete_pinjaman #url-delete').attr('action', "{{ url('pinjaman') }}/"+id);

    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil diHapus :)', {
          icon: 'success',
        });
        $(`#url-delete`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
            // var myModal = new bootstrap.Modal(document.getElementById('delete_pinjaman'))
            // myModal.show()
        });


        var max_fields = 10; // Jumlah maksimal input field yang diizinkan
        var wrapper = $(".input_no_invoice"); // Wrapper div untuk input field
        var add_button = $("#add_field"); // Tombol untuk menambah input field

        var x = 1; // Counter untuk input field

        // Ketika tombol "Add More Fields" diklik
        $(add_button).click(function(e) {
            e.preventDefault();
            if (x < max_fields) {
                x++;
                $(wrapper).append('<div class="input-field input-group"><input type="text" id="no_invoice" name="no_invoice[]" class="form-control" placeholder="Masukkan Nomor Invoice ' + x + '" aria-describedby="button-addon2"><button class="btn btn-outline-primary remove_field" type="button" id="button-addon2">Hapus</button>'); // Menambahkan input field baru
            }
        });

        // Ketika tombol "Remove" di dalam input field diklik
        $(wrapper).on("click", ".remove_field", function(e) {
            e.preventDefault();
            $(this).parent('div').remove(); // Menghapus input field
            x--; // Mengurangi counter
        });

        // Sembunyikan tombol "Remove" pada input field pertama
        $(wrapper).find('.input-field:first .remove_field').hide();
    });

</script>
@endpush
