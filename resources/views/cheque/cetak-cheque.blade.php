<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Cheque</title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 10px;
        margin-right: auto;
    }

    th {
        border: 1px solid;
    }

</style>
<body>
    <img src="{{ public_path('assets/img/logo1.jpeg') }}" height="10%">
    <div style="text-align: center;font-size:18px;margin-top:10px;margin-bottom:20px;">
        <span><strong> <u>Cheque</u></strong></span><br>
    </div>
    <p>Pada hari ini, {{ \Carbon\Carbon::now()->translatedFormat('d F Y') }}, telah dilakukan serah terima cheque fisik berupa dokumen jaminan dengan perincian sebagai berikut :</p>

    <table class="tbl-center">
        <tr>
            <td>Tanggal Cheque</td>
            <td> :</td>
            <td>{{ \Carbon\Carbon::parse($cheque->tgl_cheque)->translatedFormat('d F Y') }}</td>
        </tr>
        <tr>
            <td>Tipe</td>
            <td> :</td>
            <td>{{ $cheque->tipe_cheque }}</td>
        </tr>        
        <tr>
            <td>Bank</td>
            <td> :</td>
            <td>{{ $cheque->bank }}</td>
        </tr>        
        <tr>
            <td>Nomor Rekening</td>
            <td> :</td>
            <td>{{ $cheque->no_rek }}</td>
        </tr>        
        <tr>
            <td>Rekening Tujuan</td>
            <td> :</td>
            <td>{{ $cheque->rek_tujuan }}</td>
        </tr>        
        <tr>
            <td>Atas Nama</td>
            <td> :</td>
            <td>{{ $cheque->atas_nama }}</td>
        </tr>        
        <tr>
            <td>Nominal Cheque</td>
            <td> :</td>
            <td>@duit($cheque->nominal_cheque)</td>
        </tr>  
        <tr>
            <td>Tanggal Terima Dari</td>
            <td> :</td>
            <td>{{ \Carbon\Carbon::parse($cheque->tgl_terima_cheque)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>Materai</td>
            <td> :</td>
            <td>{{ $cheque->materai }}</td>
        </tr>        
        <tr>
            <td>Tanggal Materai</td>
            <td> :</td>
            <td>{{ \Carbon\Carbon::parse($cheque->tgl_materai)->translatedFormat('d F Y') }}</td>
        </tr>        
        <tr>
            <td>Cap Perusahaan</td>
            <td> :</td>
            <td>{{ $cheque->cap_perusahaan }}</td>
        </tr>        
        <tr>
            <td>Tanda Tangan Peminjam</td>
            <td> :</td>
            <td>{{ $cheque->ttd_borrower }}</td>
        </tr>        
        <tr>
            <td>Diterima Dari</td>
            <td> :</td>
            <td>{{ $cheque->diterima_dari }}</td>
        </tr>        

    </table>

    <p>Demikian tanda terima ini dibuat dengan sebenarnya dan dapat di pertanggung jawabkan oleh kedua belah pihak yang terlibat.</p>
    <p style="padding-left: 530px">Jakarta,_________20_____</p>

    <table style="margin-top: 10px">
        <tr>
            <td style="padding-left: 12px;">
                <div style="text-align: center;">
                    <span>Diserahkan Oleh,</span>
                    <p style="margin-top: 80px; margin-left: 13px">(__________________)</p>
                </div>
            </td>
            <td style="padding-right: 260px;">   </td>
            <td>
                <div style="text-align: center">
                    <span>Diterima Oleh,</span><br>
                    <p style="margin-top: 80px">(_______________, Credit Operation)</p>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>