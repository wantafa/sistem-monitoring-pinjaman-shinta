<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Serah Terima Cheque</title>
</head>
<style>
    .center {
        text-align: center;
    }

    .tbl-center {
        margin-left: 10px;
        margin-right: auto;
    }

    th {
        border: 1px solid;
    }

</style>
<body>
    <img src="{{ public_path('assets/img/logo1.jpeg') }}" height="10%">
    <div style="text-align: center;font-size:18px;margin-top:10px;margin-bottom:20px;">
        <span><strong> <u>Serah Terima Cheque</u></strong></span><br>
        {{-- <span class="center" >2023</span> --}}
    </div>


    {{-- <p style="font-weigth: bold;"><b>PINJAMAN</b></p> --}}
    <p>Pada hari ini, {{ \Carbon\Carbon::now()->translatedFormat('d F Y') }}, telah dilakukan serah terima cheque fisik berupa dokumen jaminan dengan perincian sebagai berikut :</p>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0; width: 100%}
        .tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
          overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
          font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
        .tg .tg-0lax{text-align:center;vertical-align:top}
        .tg .tg-isi{text-align:left;vertical-align:top}
        </style>
        <table class="tg">
        <thead>
          <tr>
            <th class="tg-0lax">No</th>
            <th class="tg-0lax">Tanggal Cheque</th>
            <th class="tg-0lax">Tipe</th>
            <th class="tg-0lax">Bank</th>
            <th class="tg-0lax">No Rekening</th>
            <th class="tg-0lax">Tempat Pembayaran</th>
            <th class="tg-0lax">Rekening Tujuan</th>
            <th class="tg-0lax">Atas Nama</th>
            <th class="tg-0lax">Nominal Cheque</th>
            <th class="tg-0lax">Tanggal Terima Dari</th>
            <th class="tg-0lax">Materai</th>
            <th class="tg-0lax">Tanggal Materai</th>
            <th class="tg-0lax">Cap Perusahaan</th>
            <th class="tg-0lax">Tanda Tangan Pemninjam</th>
            <th class="tg-0lax">Diterima Dari</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($cheque as $item)
                <tr>
                    <td class="tg-isi">{{ $no++ }}</td>
                    <td class="tg-isi">{{ \Carbon\Carbon::parse($item->tgl_cheque)->translatedFormat('d F Y') }}</td>
                    <td class="tg-isi">{{ $item->tipe_cheque }}</td>
                    <td class="tg-isi">{{ $item->bank }}</td>
                    <td class="tg-isi">{{ $item->no_rek }}</td>
                    <td class="tg-isi">{{ $item->tempat_pembayaran }}</td>
                    <td class="tg-isi">{{ $item->rek_tujuan }}</td>
                    <td class="tg-isi">{{ $item->atas_nama }}</td>
                    <td class="tg-isi">@duit($item->nominal_cheque)</td>
                    <td class="tg-isi">{{ \Carbon\Carbon::parse($item->tgl_terima_dari)->translatedFormat('d F Y') }}</td>
                    <td class="tg-isi">{{ $item->materai }}</td>
                    <td class="tg-isi">{{ \Carbon\Carbon::parse($item->tgl_materai)->translatedFormat('d F Y') }}</td>
                    <td class="tg-isi">{{ $item->cap_perusahaan }}</td>
                    <td class="tg-isi">{{ $item->ttd_borrower }}</td>
                    <td class="tg-isi">{{ $item->diterima_dari }}</td>
                </tr>
            @endforeach
        </tbody>
        </table>

        <p>Demikian tanda terima ini dibuat dengan sebenarnya dan dapat di pertanggung jawabkan oleh kedua belah pihak yang terlibat.</p>
        <p style="padding-left: 840px">Jakarta,_________20_____</p>

        <table style="margin-top: 10px">
            <tr>
                <td style="padding-left: 12px;">
                    <div style="text-align: center;">
                        <span>Diserahkan Oleh,</span>
                        <p style="margin-top: 80px; margin-left: 13px">(__________________)</p>
                    </div>
                </td>
                <td style="padding-right: 500px;">   </td>
                <td>
                    <div style="text-align: center">
                        <span>Diterima Oleh,</span><br>
                        <p style="margin-top: 80px">(_______________, Credit Operation)</p>
                    </div>
                </td>
            </tr>
        </table>
</body>
</html>