@extends('backend.index')
@section('title', 'Data Cheque')
@section('isi-konten')
<!-- Content -->
<div class="container-xxl flex-grow-1 container-p-y">

    <h4 class="fw-bold py-3 mb-4">
        <span class="text-muted fw-light">Dashboard /</span> Data Cheque
    </h4>

    @if (Auth::user()->role == 'vt')
        <button class="btn btn-primary add-btn mb-3" data-toggle="modal" data-target="#modal_cheque"> <i class="bx bx-plus bx-xs"></i> Tambah</button>
@endif
    <!-- Bordered Table -->
    <div class="card">
        <h5 class="card-header">Data Cheque</h5>
        <div class="card-body">
            <div class="table-responsive text-nowrap">
                <table class="table table-bordered" id="my-datatable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Peminjam</th>
                            <th>Tipe</th>
                            <th>Bank</th>
                            <th>Nomor Rekening</th>
                            <th>Tempat Pembayaran</th>
                            @if (Auth::user()->role == 'vt')
                            <th width="10">Actions</th>
                            @endif
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!--/ Bordered Table -->
</div>
<!-- / Content -->

{{-- Modal start --}}

{{-- <div id="modal_cheque" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><span>Tambah</span> Risk Register</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                <form method="POST" action="">
                    @csrf
                    <input type="hidden" id="id" name="id">
                    <div class="form-group">
                        <label class="col-form-label">Nama<span class="text-danger">*</span></label>
                        <input class="form-control" id="nama" name="nama" type="text" required>
                    </div>
                    <div class="form-group mb-3">
                        <label class="col-form-label">Kategori<span class="text-danger">*</span></label>
                        <input class="form-control" id="kategori" name="kategori" type="text" required>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="modal_cheque" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="modalCenterTitle"><span>Tambah</span> Cheque</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form method="POST" action="{{ route('cheque.store') }}">
                @csrf
                <input type="hidden" id="id" name="id">
                <div class="row">
                    <div class="col-md-6 mb-2">
                        <label for="peminjam_id" class="form-label">Nama Perusahaan<span class="text-danger">*</span></label>
                        <select name="peminjam_id" id="peminjam_id" class="form-control">
                            <option value="">- Pilih -</option>
                            @foreach ($peminjam as $item)
                                <option value="{{ $item->id }}">{{ $item->nama_peminjam }}</option>
                            @endforeach

                        </select>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="tipe_cheque" class="form-label">Tipe<span class="text-danger">*</span></label>
                        <select name="tipe_cheque" id="tipe_cheque" class="form-control">
                            <option value="">- Pilih -</option>
                            <option value="Giro">Giro</option>
                            <option value="Cek">Cek</option>
                        </select>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="bank" class="form-label">Bank<span class="text-danger">*</span></label>
                        <input type="text" id="bank" name="bank" class="form-control" placeholder="Masukkan Bank" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="no_rek" class="form-label">Nomor Rekening<span class="text-danger">*</span></label>
                        <input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" id="no_rek" name="no_rek" class="form-control" placeholder="Masukkan Nomor Rekening" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="tempat_pembayaran" class="form-label">Tempat Pembayaran<span class="text-danger">*</span></label>
                        <input type="text" id="tempat_pembayaran" name="tempat_pembayaran" class="form-control" placeholder="Masukkan Tempat Pembayaran" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="rek_tujuan" class="form-label">Rekening Tujuan<span class="text-danger">*</span></label>
                        <input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" id="rek_tujuan" name="rek_tujuan" class="form-control" placeholder="Masukkan Rekening Tujuan" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="atas_nama" class="form-label">Kepada Atas Nama<span class="text-danger">*</span></label>
                        <input type="text" id="atas_nama" name="atas_nama" class="form-control" placeholder="Masukkan Kepada Atas Nama" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="tgl_cheque" class="form-label">Tanggal Cheque<span class="text-danger">*</span></label>
                        <input type="date" id="tgl_cheque" name="tgl_cheque" class="form-control" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="nominal_cheque" class="form-label">Nominal Cheque<span class="text-danger">*</span></label>
                        <input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" id="nominal_cheque" name="nominal_cheque" class="form-control" placeholder="Masukkan Nominal Cheque" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="materai" class="form-label">Materai<span class="text-danger">*</span></label>
                        <input type="text" id="materai" name="materai" class="form-control" placeholder="Masukkan Materai" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="tgl_materai" class="form-label">Tanggal Materai<span class="text-danger">*</span></label>
                        <input type="date" id="tgl_materai" name="tgl_materai" class="form-control" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="cap_perusahaan" class="form-label">Cap Perusahaan<span class="text-danger">*</span></label>
                        <select name="cap_perusahaan" id="cap_perusahaan" class="form-control">
                            <option value="">- Pilih -</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="ttd_borrower" class="form-label">Tanda Tangan Borrower<span class="text-danger">*</span></label>
                        <select name="ttd_borrower" id="ttd_borrower" class="form-control">
                            <option value="">- Pilih -</option>
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                        </select>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="tgl_terima_cheque" class="form-label">Tanggal Terima Cheque<span class="text-danger">*</span></label>
                        <input type="date" id="tgl_terima_cheque" name="tgl_terima_cheque" class="form-control" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="diterima_dari" class="form-label">Diterima Dari<span class="text-danger">*</span></label>
                        <input type="text" id="diterima_dari" name="diterima_dari" class="form-control" placeholder="Masukkan Diterima Dari" required>
                    </div>
                    <div class="col-md-6 mb-2">
                        <label for="status" class="form-label">Status<span class="text-danger">*</span></label>
                        <select name="status" id="status" class="form-control">
                            <option value="">- Pilih -</option>
                            <option value="CO VT">CO VT</option>
                            <option value="Finance">Finance</option>
                        </select>
                    </div>
                </div>
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Tutup</button>
                    <button class="btn btn-primary">Simpan</button>
                </div>
            </form>
    </div>
  </div>

{{-- Modal end --}}

{{-- Delete Job Modal start --}}

<div class="modal custom-modal fade" id="delete_cheque" role="dialog">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="modal-header">
                    <h2 class="modal-title" id="exampleModalLongTitle">Hapus</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <h4 class="text-center">Anda yakin ingin hapus?</h4>
                <div class="modal-btn delete-action">
                    <div class="row">
                        <div class="col-6">
                            <form method="POST" id="url-delete">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-primary continue-btn w-100" type="submit">Delete</button>
                            </form>
                        </div>
                        <div class="col-6">
                            <button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Delete Job Modal end --}}

{{-- modal end --}}
@endsection
@push ('page-scripts')
{{-- <script>
        var table = $("#my-datatable").DataTable();

</script> --}}

<script>
    $(document).ready(function() {
        var table = $("#my-datatable").DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('cheque.json') }}",
            columns: [
                {
                    data: "DT_RowIndex",
                    name: "DT_RowIndex",
                },
                {
                    data: "nama_peminjam",
                    name: "nama_peminjam",
                    // render: function(data, type, row) {
                    //     var url = "{{ url('pkpt/risk-register') }}/"+row.id;
                    //     return `<a href="${url}">${data}</a>`;
                    // }
                },
                {
                    data: "tipe_cheque",
                    name: "tipe_cheque"
                },
                {
                    data: "bank",
                    name: "bank"
                },
                {
                    data: "no_rek",
                    name: "no_rek"
                },
                {
                    data: "tempat_pembayaran",
                    name: "tempat_pembayaran"
                },
                @if (Auth::user()->role == 'vt')
                {
                    data: "action",
                    name: "action",
                    className: 'text-center'
                },
                @endif
            ],
            order: [[ 0, "desc" ]]
        });

        table.on('draw.dt', function () {
            var info = table.page.info();
            table.column(0, {
                search: 'applied', order: 'applied', page: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1 + info.start;
            });
        });

        // add Peminjam
        $('.add-btn').on('click', function(){            
            $('#modal_cheque .modal-title span').html('Tambah');
            $('#modal_cheque #id').val('');
            $('#modal_cheque form').trigger('reset');


            var myModal = new bootstrap.Modal(document.getElementById('modal_cheque'))
            myModal.show()
        });

        // edit Peminjam
        $("#my-datatable").on("click", ".btn-edit", function(e) {
            e.preventDefault();
            data = $(this).data();

            $.each(data, function(index, value){
                
                if (index === 'peminjam_id') {
                    $('#modal_cheque #'+index).val(value).change();

                } else {
                        $('#modal_cheque #'+index).val(value);
                }
                // $('#modal_cheque #'+index).val(value);
            })

            $('#modal_cheque'+' .modal-title span').html('Edit');

            var myModal = new bootstrap.Modal(document.getElementById('modal_cheque'))
            myModal.show()
        });

        // delete Peminjam
        $("#my-datatable").on("click", ".btn-delete", function(e) {
            e.preventDefault();
            const id = $(this).data("id");
            $('#delete_cheque #url-delete').attr('action', "{{ url('cheque') }}/"+id);

    swal({
        title: 'Kamu Yakin Mau Hapus?',
        text: 'Jika di Hapus, Data akan hilang!',
        icon: 'warning',
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
        swal('Data Berhasil diHapus :)', {
          icon: 'success',
        });
        $(`#url-delete`).submit();
        } else {
        swal('Data Kamu Aman!');
        }
      });
            // var myModal = new bootstrap.Modal(document.getElementById('delete_cheque'))
            // myModal.show()
        });

    });
</script>
@endpush
