<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cheque extends Model
{
    use HasFactory;

    protected $table = 'cheque';
    protected $fillable = [
        'peminjam_id',
        'tipe_cheque',
        'bank',
        'no_rek',
        'tempat_pembayaran',
        'rek_tujuan',
        'atas_nama',
        'tgl_cheque',
        'nominal_cheque',
        'materai',
        'tgl_materai',
        'cap_perusahaan',
        'ttd_borrower',
        'tgl_terima_cheque',
        'diterima_dari',
        'status'
    ];
}
