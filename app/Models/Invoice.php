<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $table = 'invoice';
    protected $fillable = [
        'pinjaman_id',
        'tgl_pembayaran',
        'no_invoice',
        'nominal_invoice',
        'note',
        'status',
    ];
}
