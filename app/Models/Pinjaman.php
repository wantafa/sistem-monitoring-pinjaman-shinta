<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pinjaman extends Model
{
    use HasFactory;

    protected $table = 'pinjaman';
    protected $fillable = [
        'user_id',
        'peminjam_id',
        'tipe_pinjaman_id',
        'no_pinjaman',
        'tgl_pencairan',
        'tgl_jatuh_tempo',
        'tgl_pembayaran',
        'tgl_validasi',
        'nominal_pinjaman',
        'nama_payor',
        'no_invoice',
        'dokumen',
        'status_pembayaran',
        'created_by',
    ];
}
