<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Riwayat;
use App\Models\Peminjam;
use App\Models\Pinjaman;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Auth;

class VerificationController extends Controller
{

    public function dataJson_peminjam()
    {
        return DataTables::of(Peminjam::orderByDesc('id')->get())
            ->addColumn('action', function ($row) {
               $lihat= '<a href="'.route("peminjam.verif", $row->id).'" class="btn btn-md me-1"><i class="bx bxs-show"></i></a>';

                // $action = '<a href="javascript:void(0);" class="btn btn-md btn-edit" data-id="' . $row->id . '" data-nama_peminjam="' . $row->nama_peminjam . '" data-alamat="' . $row->alamat . '"><i class="bx bxs-edit"></i></a> <a href="javascript:void(0);" data-id="' . $row->id . '" class="btn btn-md btn-delete"><i class="bx bxs-trash"></i></a>';

                return $lihat;
            })
            ->addIndexColumn()
            ->make(true);
    }

    public function dataJson_pinjaman($id)
    {
        $today = Carbon::now();

        return DataTables::of(Pinjaman::where('peminjam_id', $id)
        ->whereBetween('tgl_jatuh_tempo', [$today, $today->copy()->addDays(7)])
        ->join('tipe_pinjaman', 'pinjaman.tipe_pinjaman_id', '=', 'tipe_pinjaman.id')
        ->join('peminjam', 'pinjaman.peminjam_id', '=', 'peminjam.id')
        ->where('status_pembayaran', 'Pending')
        ->select('pinjaman.id as id', 'pinjaman.*', 'tipe_pinjaman.nama', 'tipe_pinjaman.kode', 'peminjam.nama_peminjam')
        ->orderByDesc('pinjaman.id')->get())
        
        ->addColumn('action', function ($row) {
                if (Auth::user()->role == 'vt') {
                    $action = '
                    <a href="javascript:void(0);"
                    data-no_pinjaman="'.$row->no_pinjaman.'" 
                    data-nama_peminjam="'.$row->nama_peminjam.'" 
                    data-nama="'.$row->nama.'" 
                    data-tgl_pencairan="'.Carbon::parse($row->tgl_pencairan)->translatedformat('d F Y').'" 
                    data-tgl_jatuh_tempo="'.Carbon::parse($row->tgl_jatuh_tempo)->translatedformat('d F Y').'" 
                    data-nominal_pinjaman="'.$row->nominal_pinjaman.'" 
                    data-nama_payor="'.$row->nama_payor.'" 
                    data-no_invoice="'.$row->no_invoice.'" 
                    data-dokumen="'.$row->dokumen.'" 
                    data-tgl_validasi="'.Carbon::parse($row->tgl_validasi)->translatedformat('d F Y').'" 
                    data-note="'.$row->note.'" 
                    class="btn btn-md btn-detail"><i class="bx bxs-show"></i></a>
                    
                    <a href="javascript:void(0);" class="btn btn-md btn-edit" data-id="' . $row->id .'" data-tgl_validasi="' . $row->tgl_validasi .'" data-status_pembayaran="' . $row->status_pembayaran .'" data-tipe_pinjaman_id="' . $row->tipe_pinjaman_id . '" data-kode="' . $row->kode . '" data-no_invoice="' . $row->no_invoice . '"><i class="bx bxs-edit"></i></a>';

                    return $action;
                }
            })
            ->addColumn('tgl_pencairan', function ($row) {
                $tgl = Carbon::parse($row->tgl_pencairan)->translatedFormat('d F Y');
                    return $tgl;
            })
            ->addColumn('tgl_jatuh_tempo', function ($row) {
                $tgl = Carbon::parse($row->tgl_pencairan)->translatedFormat('d F Y');
                    return $tgl;
            })
            ->addIndexColumn()
            ->make(true);

    }

    public function index()
    {
        return view('verification.index_peminjam');
    }

    public function index_pinjaman($id)
    {
        $peminjam = Peminjam::find($id);

        return view('verification.index', compact('peminjam'));
    }

    public function verif(Request $request)
    {
        $pinjaman = Pinjaman::find($request->id);

        $req_form = $request->all();

        
        $req_form['user_id'] = Auth::user()->id;
        $req_form['posisi'] = 'Pending Verification';
        $req_form['pinjaman_id'] = $pinjaman->id;
        
        $pinjaman->update($req_form);
        $riwayat = Riwayat::create($req_form);

        return back()->with('success', 'Verification Berhasil Disimpan');
    }
}
