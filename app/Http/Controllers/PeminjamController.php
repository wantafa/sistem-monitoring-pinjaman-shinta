<?php

namespace App\Http\Controllers;

use App\Models\Peminjam;
use App\Models\Pinjaman;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class PeminjamController extends Controller
{
    public function dataJson()
    {
        return DataTables::of(Peminjam::orderByDesc('id')->get())
            ->addColumn('action', function ($row) {
                // <a href="'.route("peminjam.show", $row->id).'" class="btn btn-info shadow btn-md me-1"><i class="fa fa-eye text-white"></i></a>

                $action = '<a href="javascript:void(0);" class="btn btn-md btn-edit" data-id="' . $row->id . '" data-nama_peminjam="' . $row->nama_peminjam . '" data-nama_direktur="' . $row->nama_direktur . '" data-no_telp="' . $row->no_telp . '" data-email="' . $row->email . '" data-jabatan="' . $row->jabatan .'" data-alamat="' . $row->alamat . '"><i class="bx bxs-edit"></i></a> <a href="javascript:void(0);" data-id="' . $row->id . '" class="btn btn-md btn-delete"><i class="bx bxs-trash"></i></a>';
                return $action;
            })
            ->addIndexColumn()
            ->make(true);
    }
   
    public function dataJson_pinjaman()
    {
        return DataTables::of(Peminjam::orderByDesc('id')->get())
            ->addColumn('action', function ($row) {
               $lihat= '<a href="'.route("peminjam.pinjaman", $row->id).'" class="btn btn-md me-1"><i class="bx bxs-show"></i></a>';

                // $action = '<a href="javascript:void(0);" class="btn btn-md btn-edit" data-id="' . $row->id . '" data-nama_peminjam="' . $row->nama_peminjam . '" data-alamat="' . $row->alamat . '"><i class="bx bxs-edit"></i></a> <a href="javascript:void(0);" data-id="' . $row->id . '" class="btn btn-md btn-delete"><i class="bx bxs-trash"></i></a>';

                return $lihat;
            })
            ->addIndexColumn()
            ->make(true);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('peminjam.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_peminjam' => 'required',
            'alamat' => 'required',
        ]);

        $req_form = $request->all();

        if ($request->id) {

            $peminjam = Peminjam::find($request->id);

            $peminjam->update($req_form);
            $message = "Data Peminjam Berhasil diupdate";


        } else {

            $peminjam = Peminjam::create($req_form);
            $message = "Data Peminjam Berhasil Disimpan";

        }

        return back()->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function show(Peminjam $peminjam)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function edit(Peminjam $peminjam)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peminjam $peminjam)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peminjam = Peminjam::find($id);
        $peminjam->delete();

        return back();
    }
}
