<?php

namespace App\Http\Controllers;

use Auth;
use Carbon\Carbon;
use App\Models\Cheque;
use App\Models\Invoice;
use App\Models\Peminjam;
use App\Models\Pinjaman;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class PinjamanController extends Controller
{
    public function dataJson($id)
    {
        return DataTables::of(Pinjaman::where('peminjam_id', $id)->join('tipe_pinjaman', 'pinjaman.tipe_pinjaman_id', '=', 'tipe_pinjaman.id')->select('pinjaman.id as id', 'pinjaman.*', 'tipe_pinjaman.nama', 'tipe_pinjaman.kode')->orderByDesc('pinjaman.id')->get())
            ->addColumn('action', function ($row) {
                if (Auth::user()->role == 'disbur') {
                // <a href="'.route("user.show", $row->id).'" class="btn btn-info shadow btn-md me-1"><i class="fa fa-eye text-white"></i></a>
                    $action = '<a href="javascript:void(0);" class="btn btn-md btn-edit" data-id="' . $row->id . '" data-user_id="' . $row->user_id . '" data-peminjam_id="' . $row->peminjam_id . '" data-tipe_pinjaman_id="' . $row->tipe_pinjaman_id . '" data-nama="' . $row->nama . '" data-no_pinjam="' . $row->no_pinjam . '" data-tgl_pencairan="' . $row->tgl_pencairan . '" data-tgl_jatuh_tempo="' . $row->tgl_jatuh_tempo . '" data-tgl_pembayaran="' . $row->tgl_pembayaran . '" data-nominal_pinjaman="' . $row->nominal_pinjaman . '" data-no_invoice="' . $row->no_invoice . '"><i class="bx bxs-edit"></i></a> 
                    
                    <a href="javascript:void(0);" data-id="' . $row->id . '" class="btn btn-md btn-delete"><i class="bx bxs-trash"></i></a>';
                    return $action;
                }
            })
            ->addColumn('tgl_pencairan', function ($row) {
                $tgl = Carbon::parse($row->tgl_pencairan)->translatedFormat('d F Y');
                    return $tgl;
            })
            ->addColumn('tgl_jatuh_tempo', function ($row) {
                $tgl = Carbon::parse($row->tgl_pencairan)->translatedFormat('d F Y');
                    return $tgl;
            })
            ->addIndexColumn()
            ->make(true);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pinjaman.index_peminjam');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_pinjaman($id)
    {
        $peminjam = Peminjam::find($id);

        $tipe_pinjaman = DB::table('tipe_pinjaman')->select('*')->get();

        return view('pinjaman.index', compact('peminjam', 'tipe_pinjaman'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'name' => 'required',
        //     'divisi' => 'required',
        // ]);

        $req_form = $request->all();

        $req_form['no_invoice'] = implode(',', $request->no_invoice);
        
        $req_form['user_id'] = Auth::user()->id;

        if ($request->id) {

            $pinjam = Pinjaman::find($request->id);

            $image = $pinjam->dokumen;

        if ($request->hasFile('dokumen')) {
            File::delete($pinjam->dokumen);
            
            $image = 'uploads/images/pinjam/'. time() . '_' . $request->file('dokumen')->getClientOriginalName();
            $request->file('dokumen')->move('uploads/images/pinjam', $image);

        }
            $req_form['dokumen'] = $image;

            $count_pinjaman = Pinjaman::where('tipe_pinjaman_id', $request->tipe_pinjaman_id)->count();

            $tipe_pinjaman = DB::table('tipe_pinjaman')->where('id', $request->tipe_pinjaman_id)->first();
            
                $existing_pinjaman = Pinjaman::where('tipe_pinjaman_id', $request->tipe_pinjaman_id)->pluck('no_pinjaman');
                $existing_numbers = [];

                foreach ($existing_pinjaman as $pinjaman) {
                    $number = explode('/', $pinjaman)[1];
                    $existing_numbers[] = intval($number);
                }
                
                $no_baru = null;
                
                if ($pinjam->tipe_pinjaman_id != $request->tipe_pinjaman_id)  {

                    for ($i = 1; $i <= $count_pinjaman; $i++) {
                        if (!in_array($i, $existing_numbers)) {
                            $no_baru = $i;
                            break;
                        }
                    }
                } else {
                        $no_tetap = explode('/', $pinjam->no_pinjaman)[1];
                        if (in_array($no_tetap, $existing_numbers)) {
                            $no_baru = $no_tetap;
                        }
                }

                if ($no_baru === null) {
                    $no_baru = $count_pinjaman + 1;

                }
                

            $req_form['no_pinjaman'] = $tipe_pinjaman->kode . '/' . $no_baru;

            $pinjam->update($req_form);
            $message = "Data Pinjaman Berhasil diupdate";

        } else {

            if ($request->hasFile('dokumen')) {
                $image = 'uploads/images/pinjam/'. time() . '_' . $request->file('dokumen')->getClientOriginalName(); 
                $request->file('dokumen')->move('uploads/images/pinjam', $image);
                
                $req_form['dokumen'] = $image;
            }

            $req_form['created_by'] = Auth::user()->id;
            $req_form['status_pembayaran'] = 'Pending';

            $count_pinjaman = Pinjaman::where('tipe_pinjaman_id', $request->tipe_pinjaman_id)->count();
        
            $tipe_pinjaman = DB::table('tipe_pinjaman')->where('id', $request->tipe_pinjaman_id)->first();
            
            $req_form['no_pinjaman'] =  $tipe_pinjaman->kode . '/' . ($count_pinjaman + 1);

            $pinjam = Pinjaman::create($req_form)->id;

            foreach($request->input('no_invoice') as $key => $item) {

                Invoice::create([
                    'pinjaman_id' => $pinjam,
                    'no_invoice' => $item,
                ]);
            }
            $message = "Data Pinjaman Berhasil Disimpan";

        }

        return back()->with('success', $message);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function laporan(Request $request)
    {

        $no = 1;
        $get_peminjam = Peminjam::all();
        $get_pinjaman = Pinjaman::all();
        $get_cheque   = Cheque::all();
        $pinjaman     = [];
        $cheque     = [];

        if ($request->pinjaman == "verif") {
            
            $today = Carbon::now();
            
            $pinjaman = Pinjaman::whereBetween('tgl_jatuh_tempo', [$today, $today->copy()->addDays(7)])
            ->join('tipe_pinjaman', 'pinjaman.tipe_pinjaman_id', '=', 'tipe_pinjaman.id')
            ->join('peminjam', 'pinjaman.peminjam_id', '=', 'peminjam.id')
            ->where('peminjam_id', $request->peminjam_id)
            ->where('status_pembayaran', 'Pending')
            ->select('pinjaman.id as id', 'pinjaman.*', 'tipe_pinjaman.nama', 'tipe_pinjaman.kode', 'peminjam.nama_peminjam')
            ->orderByDesc('pinjaman.id')->get();

        } else if($request->pinjaman == "pinjaman") {

            $pinjaman = Pinjaman::where('peminjam_id', $request->peminjam_id)
            ->join('tipe_pinjaman', 'pinjaman.tipe_pinjaman_id', '=', 'tipe_pinjaman.id')
            ->join('peminjam', 'pinjaman.peminjam_id', '=', 'peminjam.id')
            ->select('pinjaman.id as id', 'pinjaman.*', 'tipe_pinjaman.nama', 'tipe_pinjaman.kode', 'peminjam.nama_peminjam')
            ->orderByDesc('pinjaman.id')->get();
            
        } else if($request->pinjaman == "pending") {

            $pinjaman = Pinjaman::join('tipe_pinjaman', 'pinjaman.tipe_pinjaman_id', '=', 'tipe_pinjaman.id')
            ->join('peminjam', 'pinjaman.peminjam_id', '=', 'peminjam.id')
            ->where('peminjam_id', $request->peminjam_id)
            ->where('tgl_jatuh_tempo', $request->tgl_jatuh_tempo)
            ->where('status_pembayaran', 'Pending')
            ->select('pinjaman.id as id', 'pinjaman.*', 'tipe_pinjaman.nama', 'tipe_pinjaman.kode', 'peminjam.nama_peminjam')
            ->orderByDesc('pinjaman.id')->get();

        } else if($request->pinjaman == "paid") {

            $pinjaman = Pinjaman::join('tipe_pinjaman', 'pinjaman.tipe_pinjaman_id', '=', 'tipe_pinjaman.id')
            ->join('peminjam', 'pinjaman.peminjam_id', '=', 'peminjam.id')
            ->where('peminjam_id', $request->peminjam_id)
            ->where('tgl_pembayaran', $request->tgl_pembayaran)
            ->where('status_pembayaran', 'Paid')
            ->select('pinjaman.id as id', 'pinjaman.*', 'tipe_pinjaman.nama', 'tipe_pinjaman.kode', 'peminjam.nama_peminjam')
            ->orderByDesc('pinjaman.id')->get();

        } else if($request->pinjaman == "cheque") {

            $cheque = Cheque::join('peminjam', 'cheque.peminjam_id', '=', 'peminjam.id')
            ->where('peminjam_id', 'LIKE', "%".$request->peminjam_id."%")
            ->where('tgl_terima_cheque', 'LIKE', "%".$request->tgl_terima_cheque."%")
            ->where('nominal_cheque', 'LIKE', "%".$request->nominal_cheque."%")
            ->where('status', 'LIKE', "%".$request->status."%")->get();

        }

        return view('laporan.index', compact('pinjaman', 'cheque', 'no', 'get_pinjaman', 'get_cheque', 'get_peminjam'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function cetak_pinjaman(Request $request)
    {
        $no = 1;

        if ($request->pinjaman == "verif") {
            
            $today = Carbon::now();
            
            $pinjaman = Pinjaman::whereBetween('tgl_jatuh_tempo', [$today, $today->copy()->addDays(7)])
            ->join('tipe_pinjaman', 'pinjaman.tipe_pinjaman_id', '=', 'tipe_pinjaman.id')
            ->join('users', 'pinjaman.user_id', '=', 'users.id')
            ->where('peminjam_id', $request->peminjam_id)
            ->where('status_pembayaran', 'Pending')
            ->select('pinjaman.id as id', 'pinjaman.*', 'tipe_pinjaman.nama', 'tipe_pinjaman.kode', 'users.name as nama_peminjam')
            ->orderByDesc('pinjaman.id')->get();

        } else if($request->pinjaman == "pinjaman") {

            $pinjaman = Pinjaman::where('peminjam_id', $request->peminjam_id)->get();
            
        } else if($request->pinjaman == "pending") {

            $pinjaman = Pinjaman::where('peminjam_id', $request->peminjam_id)->where('tgl_jatuh_tempo', $request->tgl_jatuh_tempo)->get();

        } else if($request->pinjaman == "paid") {

            $pinjaman = Pinjaman::where('peminjam_id', $request->peminjam_id)->where('tgl_pembayaran', $request->tgl_pembayaran)->get();

        } 

        return view('laporan.cetak-pinjaman', compact('pinjaman', 'no'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function cetak_laporan_cheque(Request $request)
    {
            $no = 1;

            $cheque = Cheque::join('peminjam', 'cheque.peminjam_id', '=', 'peminjam.id')
            ->where('peminjam_id', 'LIKE', "%".$request->peminjam_id."%")
            ->where('tgl_terima_cheque', 'LIKE', "%".$request->tgl_terima_cheque."%")
            ->where('nominal_cheque', 'LIKE', "%".$request->nominal_cheque."%")
            ->where('status', 'LIKE', "%".$request->status."%")->get();

            $pdf = Pdf::loadView('cheque.cetak-laporan', compact('cheque', 'no'))->setPaper('a4', 'landscape')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);

            return $pdf->download('Laporan Cheque.pdf');
            
            // return view('cheque.cetak-laporan', compact('cheque', 'no'));
    }
    
    public function cetak_cheque($id)
    {

            $cheque = Cheque::find($id);

            // $cheque = Cheque::where('peminjam_id', $request->peminjam_id)
            // ->where('tgl_terima_cheque', $request->tgl_terima_cheque)
            // ->where('nominal_cheque', $request->nominal_cheque)
            // ->where('status', $request->status)->get();

            $pdf = Pdf::loadView('cheque.cetak-cheque', compact('cheque'))->setPaper('a4')->setOptions(['defaultFont' => 'Times New Roman", Times, serif','chroot'  => public_path('assets/img')]);

            return $pdf->stream('Laporan Cheque.pdf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pinjaman  $pinjaman
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pinjam = Pinjaman::find($id);
        $pinjam->delete();

        return back();
    }
}
