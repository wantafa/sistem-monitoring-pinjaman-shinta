<?php

namespace App\Http\Controllers;

use App\Models\Cheque;
use App\Models\Peminjam;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Auth;

class ChequeController extends Controller
{
    public function dataJson()
    {
        return DataTables::of(Cheque::join('peminjam', 'cheque.peminjam_id', '=', 'peminjam.id')->orderByDesc('cheque.id')->get())
            ->addColumn('action', function ($row) {
                // <a href="'.route("cheque.show", $row->id).'" class="btn btn-info shadow btn-md me-1"><i class="fa fa-eye text-white"></i></a>
            
            if (Auth::user()->role == 'vt') {
                    $action = '<a href="javascript:void(0);" class="btn btn-md btn-edit" data-id="' . $row->id . '" data-peminjam_id="' . $row->peminjam_id . '" data-nama_peminjam="' . $row->nama_peminjam . '" data-tipe_cheque="' . $row->tipe_cheque . '" data-bank="' . $row->bank . '" data-no_rek="' . $row->no_rek . '" data-tempat_pembayaran="' . $row->tempat_pembayaran . '" data-rek_tujuan="' . $row->rek_tujuan . '" data-atas_nama="' . $row->atas_nama . '" data-tgl_cheque="' . $row->tgl_cheque . '" data-nominal_cheque="' . $row->nominal_cheque . '" data-materai="' . $row->materai . '" data-tgl_materai="' . $row->tgl_materai . '" data-cap_perusahaan="' . $row->cap_perusahaan . '" data-ttd_borrower="' . $row->ttd_borrower . '" data-tgl_terima_cheque="' . $row->tgl_terima_cheque . '" data-diterima_dari="' . $row->diterima_dari . '" data-status="' . $row->status .'"><i class="bx bxs-edit"></i></a> <a href="javascript:void(0);" data-id="' . $row->id . '" class="btn btn-md btn-delete"><i class="bx bxs-trash"></i></a>';
                    return $action;
                }
            })
            ->addIndexColumn()
            ->make(true);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peminjam = Peminjam::all();
        return view('cheque.index', compact('peminjam'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $request->validate([
        //     'nama_peminjam' => 'required',
        //     'alamat' => 'required',
        // ]);

        $req_form = $request->all();

        if ($request->id) {

            $cheque = Cheque::find($request->id);

            $cheque->update($req_form);
            $message = "Data Cheque Berhasil diupdate";


        } else {

            $cheque = Cheque::create($req_form);
            $message = "Data Cheque Berhasil Disimpan";

        }

        return back()->with('success', $message);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cheque  $cheque
     * @return \Illuminate\Http\Response
     */
    public function show(Cheque $cheque)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cheque  $cheque
     * @return \Illuminate\Http\Response
     */
    public function edit(Cheque $cheque)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cheque  $cheque
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cheque $cheque)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cheque  $cheque
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cheque = Cheque::find($id);
        $cheque->delete();

        return back();
    }
}
