<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Peminjam;
use App\Models\Pinjaman;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        toast('Login Berhasil', 'success')->position('center')->width('300px')->timerProgressBar()->background('#E8FADF');
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $today = Carbon::now();
        
        $peminjam = Peminjam::count();
        $pinjaman = Pinjaman::count();
        $verif    = Pinjaman::whereBetween('tgl_jatuh_tempo', [$today, $today->copy()->addDays(7)])->count();
        $pending  = Pinjaman::where('status_pembayaran', 'Pending')->count();
        $paid     = Pinjaman::where('status_pembayaran', 'Paid')->count();

        $last_empat_bulan = Carbon::now()->subMonths(4)->startOfMonth();

        $chart_pending= Pinjaman::where('status_pembayaran', 'Pending')->whereBetween('created_at', [$last_empat_bulan, $today])->orderBy('id')->count();

        $chart_paid= Pinjaman::where('status_pembayaran', 'Paid')->whereBetween('created_at', [$last_empat_bulan, $today])->orderBy('id')->count();
        
        // dd($chart_data_pending);
        return view('dashboard', compact('peminjam', 'pinjaman', 'verif', 'pending', 'paid', 'chart_pending', 'chart_paid'));
    }

}
