<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChequeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cheque', function (Blueprint $table) {
            $table->increments('id');
            $table->string('peminjam_id');
            $table->string('tipe_cheque');
            $table->string('bank');
            $table->string('no_rek');
            $table->string('tempat_pembayaran');
            $table->string('rek_tujuan');
            $table->string('atas_nama');
            $table->date('tgl_cheque');
            $table->string('nominal_cheque');
            $table->string('materai');
            $table->date('tgl_materai');
            $table->string('cap_perusahaan');
            $table->string('ttd_borrower');
            $table->date('tgl_terima_cheque');
            $table->string('diterima_dari');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cheque');
    }
}
