<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePinjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjaman', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('peminjam_id');
            $table->string('tipe_pinjaman_id');
            $table->string('no_pinjaman');
            $table->date('tgl_pencairan');
            $table->date('tgl_jatuh_tempo');
            $table->date('tgl_pembayaran');
            $table->date('tgl_validasi');
            $table->string('nominal_pinjaman');
            $table->string('nama_payor');
            $table->string('no_invoice');
            $table->string('dokumen');
            $table->string('status_pembayaran');
            $table->string('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjaman');
    }
}
