<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PaidController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ChequeController;
use App\Http\Controllers\PendingController;
use App\Http\Controllers\PeminjamController;
use App\Http\Controllers\PinjamanController;
use App\Http\Controllers\VerificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('user.content');
// });


Auth::routes();

Route::get('/', function () {
    return redirect(route('login'));
});

Route::group(['middleware' => 'auth'], function() {

Route::get('/dashboard', [HomeController::class, 'index'])->name('home');
Route::get('/profil', [UserController::class, 'profil'])->name('profil');


// DATA JSON
Route::get('/user/datajson', [UserController::class, 'dataJson'])->name('user.json');
Route::get('/peminjam/datajson', [PeminjamController::class, 'dataJson'])->name('peminjam.json');
Route::get('/peminjam/pinjaman/datajson', [PeminjamController::class, 'dataJson_pinjaman'])->name('peminjam.pinjam.json');
Route::get('/pinjaman/datajson/{id}', [PinjamanController::class, 'dataJson'])->name('pinjaman.json');
Route::get('/verification/datajson/{id}', [VerificationController::class, 'dataJson_pinjaman'])->name('verif.json');
Route::get('/verification/peminjam/datajson', [VerificationController::class, 'dataJson_peminjam'])->name('peminjam.verif.json');
Route::get('/pending/datajson/{id}', [PendingController::class, 'dataJson_pinjaman'])->name('pending.json');
Route::get('/pending/peminjam/datajson', [PendingController::class, 'dataJson_peminjam'])->name('peminjam.pending.json');
Route::get('/paid/datajson/{id}', [PaidController::class, 'dataJson_pinjaman'])->name('paid.json');
Route::get('/paid/peminjam/datajson', [PaidController::class, 'dataJson_peminjam'])->name('peminjam.paid.json');
Route::get('/cheque/datajson', [ChequeController::class, 'dataJson'])->name('cheque.json');

Route::patch('/ubah_pass', [UserController::class, 'ubah_pass'])->name('ubah_pass');
Route::get('/peminjam/pinjaman/{id}', [PinjamanController::class, 'index_pinjaman'])->name('peminjam.pinjaman');


Route::get('verification', [VerificationController::class, 'index'])->name('index.verification');
Route::post('verification/update', [VerificationController::class, 'verif'])->name('update.verif');
Route::get('/peminjam/verification/{id}', [VerificationController::class, 'index_pinjaman'])->name('peminjam.verif');

Route::get('pending', [PendingController::class, 'index'])->name('index.pending');
Route::post('pending/update', [PendingController::class, 'pending'])->name('update.pending');
Route::get('/peminjam/pending/{id}', [PendingController::class, 'index_pinjaman'])->name('peminjam.pending');

Route::get('pending', [PendingController::class, 'index'])->name('index.pending');
Route::post('pending/update', [PendingController::class, 'pending'])->name('update.pending');
Route::get('/peminjam/pending/{id}', [PendingController::class, 'index_pinjaman'])->name('peminjam.pending');

Route::get('paid', [PaidController::class, 'index'])->name('index.paid');
Route::get('/peminjam/paid/{id}', [PaidController::class, 'index_pinjaman'])->name('peminjam.paid');

Route::get('riwayat/{id}', [PaidController::class, 'riwayat'])->name('riwayat');
Route::get('invoice/{id}', [PendingController::class, 'invoice'])->name('invoice');
Route::get('pinjaman/cetak/{id}', [PaidController::class, 'cetak'])->name('cetak');



// Laporan
Route::get('laporan', [PinjamanController::class, 'laporan'])->name('laporan');
Route::get('laporan/cetak-pinjaman', [PinjamanController::class, 'cetak_pinjaman'])->name('cetak.pinjaman');
Route::get('laporan/cetak-cheque', [PinjamanController::class, 'cetak_laporan_cheque'])->name('cetak.laporan.cheque');
Route::get('laporan/cetak-cheque/{id}', [PinjamanController::class, 'cetak_cheque'])->name('cetak.cheque');


// RESOURCE
Route::resource('user', UserController::class);
Route::resource('peminjam', PeminjamController::class);
Route::resource('pinjaman', PinjamanController::class);
Route::resource('cheque', ChequeController::class);


});